from sqlalchemy.orm import sessionmaker
from common import create_engine, User, Job, TaskType, Email

def create_tables(engine):
    from common import Base
    Base.metadata.create_all(engine)

def populate_db(session):
    bob = User(name='bob', fullname='bob osmoth')
    rob = User(name='rob', fullname='rob mcface')
    terry = User(name='terry', fullname='terry jimbo')
    dig = TaskType(taskname='dig pit')
    fill = TaskType(taskname='fill pit')

    session.add_all([
        bob,
        rob,
        terry,
        dig,
        fill,
    ])
    session.commit()

    bob_email1 = Email(email="bob@example.com", owner=bob.id)
    bob_email2 = Email(email="bobhotstuff@yahoo.com", owner=bob.id)
    rob_email = Email(email="rob@example.com", owner=rob.id)

    bob_digs = Job(type=dig.id, owner=bob.id)
    bob_fills = Job(type=fill.id, owner=bob.id)
    rob_digs = Job(type=dig.id, owner=rob.id, backup=bob.id)
    no_owners = Job(type=dig.id)
    only_backup = Job(type=dig.id, backup=bob.id)
    session.add_all([
        bob_digs,
        bob_fills,
        rob_digs,
        bob_email1,
        bob_email2,
        rob_email,
        no_owners,
        only_backup,
    ])
    session.commit()

engine = create_engine()
create_tables(engine)
session = sessionmaker(bind=engine)()
populate_db(session)

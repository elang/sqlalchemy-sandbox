from common import create_engine
from sqlalchemy.orm import sessionmaker, aliased
from sqlalchemy.sql import func
from common import User, Job, TaskType, Email

def show_users(session):
    q = session.query(User)
    print('Users')
    print(q)
    for user in q:
        print(' ', user.id, user.name, user.fullname)

def show_jobs(session):
    q = session.query(User, TaskType) \
               .join(Job, User.id==Job.owner) \
               .join(TaskType, Job.type==TaskType.id) \
               .order_by(Job.id)
    print('Jobs')
    print(q)
    for user, tasktype in q:
        print(' ', user.id, user.name, tasktype.taskname)

def show_email_counts(session):
    # https://docs.sqlalchemy.org/en/latest/orm/tutorial.html#using-subqueries
    sub = session.query(Email.owner, func.count('*') \
                 .label('address_count')) \
                 .group_by(Email.owner) \
                 .subquery()

    q = session.query(User.name, sub.c.address_count) \
               .outerjoin(sub, User.id==sub.c.owner)
    print('Email')
    print(q)
    for user, count in q:
        print(' ', user, count)

def multi_join(session):
    u1 = aliased(User)
    u2 = aliased(User)
    q = session.query(Job, u1.name, u2.name) \
               .outerjoin(u1, u1.id==Job.owner) \
               .outerjoin(u2, u2.id==Job.backup)
    print('Multi join')
    print(q)
    for job, owner, backup in q:
        print(' ', job.id, owner, backup)

def correlated_subquery(session):
    JobAlias = aliased(Job)
    owner_sq = session.query(User.name) \
                      .filter(User.id==JobAlias.owner) \
                      .as_scalar()
    backup_sq = session.query(User.name) \
                       .filter(User.id==JobAlias.backup) \
                       .as_scalar()
    q = session.query(JobAlias.id, owner_sq, backup_sq) \
               .order_by(JobAlias.id)
    print('Correlated subquery')
    print(q)
    for job, owner, backup in q:
        print(' ', job, owner, backup)
 
engine = create_engine(echo=False)
session = sessionmaker(bind=engine)()
show_users(session)
show_jobs(session)
show_email_counts(session)
multi_join(session)
correlated_subquery(session)

# sqlalchemy-sandbox

Start mysql container:

```shell
docker run --name mysql -p 127.0.0.1:3306:3306 -e MYSQL_USER=user -e MYSQL_PASSWORD=password -e MYSQL_DATABASE=test -e MYSQL_ROOT_PASSWORD=password -d mysql:8.0.14
```

Access mysql shell:

```shell
docker exec -it mysql mysql -uuser -ppassword
```

Connect remotely:

```
jdbc:mysql://127.0.0.1:3306/test
```

For some client versions, may need to run:

```
ALTER USER 'user'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
```

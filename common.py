from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import create_engine as sq_create_engine
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String(32))
    fullname = Column(String(32))


class Email(Base):
    __tablename__ = 'email'
    id = Column(Integer, primary_key=True)
    email = Column(String(32))
    owner = Column(Integer, ForeignKey("user.id"))

class Job(Base):
    __tablename__ = 'job'
    id = Column(Integer, primary_key=True)
    type = Column(Integer, ForeignKey("task_type.id"))
    owner = Column(Integer, ForeignKey("user.id"))
    backup = Column(Integer, ForeignKey("user.id"))

class TaskType(Base):
    __tablename__ = 'task_type'
    id = Column(Integer, primary_key=True)
    taskname = Column(String(32))

def create_engine(echo=True):
    return sq_create_engine("mysql+mysqldb://user:password@127.0.0.1:3306/test", echo=echo)
